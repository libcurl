/* src/config.h.  Generated automatically by configure.  */
/* Define if you have the strcasecmp function.  */
/*#define HAVE_STRCASECMP 1*/

/* Define cpu-machine-OS */
#define OS "win32"

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you have the <io.h> header file.  */
#define HAVE_IO_H 1
